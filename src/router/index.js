import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import News from '../views/News.vue'
import About from '../views/About.vue'
import Members from '../views/Members.vue'
import Paper from '../views/Paper.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/News',
    name: 'News',
    component: News
  },
  {
    path: '/About',
    name: 'About',
    component: About
  },
  {
    path: '/Members',
    name: 'Members',
    component: Members
  },
  {
    path: '/Paper',
    name: 'Paper',
    component: Paper
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
